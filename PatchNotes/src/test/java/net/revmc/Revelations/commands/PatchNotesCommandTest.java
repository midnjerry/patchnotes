package net.revmc.Revelations.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import net.revmc.Revelations.PatchNoteManager;
import net.revmc.Revelations.MockObjects.MockCommand;
import net.revmc.Revelations.commands.PatchNotesCommand;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Bukkit.class)
public class PatchNotesCommandTest {
	@Mock
	private Player player;
	private PatchNoteManager noteManager;
	PatchNotesCommand commandExecutor;

	@Before
	public void setUp() throws Exception {
		noteManager = Mockito.mock(PatchNoteManager.class);
		commandExecutor = new PatchNotesCommand(noteManager);
	}

	@Test
	public void doesCommandPatchNotesOpenGUI() {
		Command cmd = new MockCommand("PatchNotes");
		String label = "PatchNotes";
		String[] args = null;
		commandExecutor.onCommand(player, cmd, label, args);
		Mockito.verify(noteManager, Mockito.times(1)).openMenu(player);
	}

	@Test
	public void WrongCommandDoesntRun() {
		Command cmd = new MockCommand("Patch");
		String label = "PatchNotes";
		String[] args = null;
		commandExecutor.onCommand(player, cmd, label, args);
		Mockito.verify(noteManager, Mockito.times(0)).openMenu(player);
	}
}