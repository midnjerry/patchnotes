package net.revmc.Revelations;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public interface ISaveFile {

	ArrayList<PatchNote> getAllPatchNoteEntries();

	void saveAllPatchNoteEntries(ArrayList<PatchNote> notes);

	void safeFile();

	void saveAccessTimes(HashMap<UUID, OffsetDateTime> accessTimes);

	HashMap<UUID, OffsetDateTime> getAccessTimes();

}