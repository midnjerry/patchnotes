package net.revmc.Revelations.gui;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import net.revmc.Revelations.PatchNote;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.powermock.api.mockito.PowerMockito;
import net.revmc.Revelations.MockObjects.MockInventory;
import net.revmc.Revelations.MockObjects.MockInventoryView;
import net.revmc.Revelations.MockObjects.MockItemStackFactory;
import net.revmc.Revelations.gui.Menu;
import net.revmc.Revelations.gui.NotesInventoryHolder;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Bukkit.class)
public class MenuGUITest {
	@Mock
	private Player player;
	private ArrayList<PatchNote> notes;
	private MockInventoryView view;
	private Menu menu;
	private MockItemStackFactory mFactory = new MockItemStackFactory();

	@Before
	public void setUp() throws Exception {
		view = new MockInventoryView(player, new MockInventory(), new MockInventory(), InventoryType.CHEST);
		player = Mockito.mock(Player.class);
		when(player.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3A"));
		when(player.getName()).thenReturn("NoobDad");
		when(player.getOpenInventory()).thenReturn(view);

		MockInventory menuInv = new MockInventory(new NotesInventoryHolder(), 56, "Updates for RevelationsMC!");
		PowerMockito.mockStatic(Bukkit.class);
		when(Bukkit.createInventory(Mockito.any(), Mockito.anyInt(), Mockito.anyString())).thenReturn(menuInv);

		setupPatchNotes();
		
		OffsetDateTime lastLogin = OffsetDateTime.of(2017, 8, 30, 3, 20, 0, 0, ZoneOffset.UTC);
		menu = new Menu(lastLogin, notes, mFactory);
	}

	private void setupPatchNotes() {
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("Hello guys!");
		notes = new ArrayList<PatchNote>();
		notes.add(new PatchNote("Item A", OffsetDateTime.of(2017, 8, 1, 3, 20, 0, 0, ZoneOffset.UTC), lore));
		notes.add(new PatchNote("Item B", OffsetDateTime.of(2017, 9, 1, 3, 20, 0, 0, ZoneOffset.UTC), lore));
		notes.add(new PatchNote("Item C", OffsetDateTime.of(2017, 8, 15, 3, 20, 0, 0, ZoneOffset.UTC), lore));
		notes.add(new PatchNote("Item D", OffsetDateTime.of(2017, 8, 10, 3, 20, 0, 0, ZoneOffset.UTC), lore));
		notes.add(new PatchNote("Item E", OffsetDateTime.of(2017, 8, 10, 3, 21, 0, 0, ZoneOffset.UTC), lore));
		Collections.sort(notes);
	}

	@Test
	public void CreatesPatchNoteIcon() {
		OffsetDateTime expectedDate = OffsetDateTime.now();
		String expectedTitle = expectedDate.toString();
		String expectedLore = "Hello this is a test message.";
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(expectedLore);
		
		PatchNote note = new PatchNote(expectedTitle, expectedDate, lore);
		ItemStack result = menu.createItemStack(note);
		ItemMeta meta = result.getItemMeta();
		assertEquals(expectedTitle, meta.getDisplayName());
		assertEquals(expectedLore, meta.getLore().get(0));
	}

	@Test
	public void CreateEnchantedBookForNewNotes() {
		ItemStack result = menu.createItemStack(notes.get(4));
		ItemMeta meta = result.getItemMeta();
		assertEquals(Material.ENCHANTED_BOOK, result.getType());
		assertEquals("Item B", meta.getDisplayName());
		assertEquals("Hello guys!", meta.getLore().get(0));
	}

	@Test
	public void CreateNonEnchantedBookForOldNotes() {
		ItemStack result = menu.createItemStack(notes.get(0));
		ItemMeta meta = result.getItemMeta();
		assertEquals(Material.BOOK, result.getType());
		assertEquals("Item A", meta.getDisplayName());
		assertEquals("Hello guys!", meta.getLore().get(0));
	}

	@Test
	public void MenuPopulatesNotesAsBookIcons() {
		menu = new Menu(OffsetDateTime.of(2017, 9, 15, 3, 20, 0, 0, ZoneOffset.UTC), notes, mFactory);
		Inventory inventory = menu.getMenu();
		for (int i = 0; i < notes.size(); i++) {
			ItemStack item = inventory.getItem(i);
			assertEquals(item.getType(), Material.BOOK);
		}
	}

	@Test
	public void MenuPopulatesNotesAsEnchantedBookIcons() {
		menu = new Menu(OffsetDateTime.of(2010, 9, 15, 3, 20, 0, 0, ZoneOffset.UTC), notes, mFactory);
		Inventory inventory = menu.getMenu();
		for (int i = 0; i < notes.size(); i++) {
			ItemStack item = inventory.getItem(i);
			assertEquals(item.getType(), Material.ENCHANTED_BOOK);
		}
	}

	@Test
	public void MenuPopulatesNotesAsMixOfBoth() {
		menu = new Menu(OffsetDateTime.of(2017, 8, 30, 3, 20, 0, 0, ZoneOffset.UTC), notes, mFactory);
		Inventory inventory = menu.getMenu();
		assertEquals(inventory.getItem(0).getType(), Material.BOOK);
		assertEquals(inventory.getItem(1).getType(), Material.BOOK);
		assertEquals(inventory.getItem(2).getType(), Material.BOOK);
		assertEquals(inventory.getItem(3).getType(), Material.BOOK);
		assertEquals(inventory.getItem(4).getType(), Material.ENCHANTED_BOOK);
	}
}