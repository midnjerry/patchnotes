package net.revmc.Revelations.commands;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import net.revmc.Revelations.PatchNote;
import net.revmc.Revelations.PatchNoteManager;
import net.revmc.Revelations.MockObjects.MockCommand;
import net.revmc.Revelations.MockObjects.MockConfig;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Bukkit.class)
public class AddNoteCommandTest {
	@Mock
	private Player player;
	private Player player2;
	private PatchNoteManager noteManager;
	AddNoteCommand commandExecutor;
	private String label = "AddNote";
	private Command command;
	String args[] = new String[] { "This", "is", "a", "Test." };
	String args2[] = new String[] { "The", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog." };
	String args3[] = new String[] { "The", "coolest", "update!", "|", "The", "quick", "brown", "fox", "jumps", "over",
			"the", "lazy", "dog." };
	String args4[] = new String[] { "|", "The", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog." };

	@Before
	public void setUp() throws Exception {
		player = Mockito.mock(Player.class);
		when(player.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3A"));
		when(player.getName()).thenReturn("NoobDad");

		player2 = Mockito.mock(Player.class);
		when(player2.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3B"));
		when(player2.getName()).thenReturn("Grezdaed");
		when(player2.isOp()).thenReturn(true);

		MockConfig config = new MockConfig(new ArrayList<PatchNote>(), new HashMap<UUID, OffsetDateTime>());
		noteManager = new PatchNoteManager(config);
		commandExecutor = new AddNoteCommand(noteManager);
		command = new MockCommand("AddNote");
	}

	@Test
	public void RejectsNullArgs() {
		String[] args = null;
		boolean result = commandExecutor.onCommand(player, command, label, args);
		assertEquals(false, result);
		assertEquals(0, noteManager.getNotes().size());
	}

	@Test
	public void RejectsEmptyArgs() {
		String[] args = new String[0];
		boolean result = commandExecutor.onCommand(player, command, label, args);
		assertEquals(false, result);
		assertEquals(0, noteManager.getNotes().size());
	}

	@Test
	public void DoesCommandAddNoteSendMessage() {
		boolean result = commandExecutor.onCommand(player, command, label, args);
		assertEquals(true, result);
		assertEquals(1, noteManager.getNotes().size());
	}

	@Test
	public void ProperlyParsesStringInput() {
		boolean result = commandExecutor.onCommand(player, command, label, args2);
		assertEquals(true, result);
		assertEquals(1, noteManager.getNotes().size());
		PatchNote notes = noteManager.getNotes().get(0);
		assertEquals("The quick brown fox jumps", notes.getLore().get(0));
		assertEquals("over the lazy dog.", notes.getLore().get(1));
	}

	@Test
	public void ProperlyParsesTitleInput() {
		boolean result = commandExecutor.onCommand(player, command, label, args3);
		assertEquals(true, result);
		assertEquals(1, noteManager.getNotes().size());
		PatchNote notes = noteManager.getNotes().get(0);
		assertEquals("The coolest update!", notes.getTitle());
		assertEquals("The quick brown fox jumps", notes.getLore().get(0));
		assertEquals("over the lazy dog.", notes.getLore().get(1));
	}

	@Test
	public void ProperlyHandlesEmptyTitleInput() {
		boolean result = commandExecutor.onCommand(player, command, label, args4);
		assertEquals(true, result);
		assertEquals(1, noteManager.getNotes().size());
		PatchNote notes = noteManager.getNotes().get(0);
		assertEquals(true, notes.getTitle().contains("Update"));
		assertEquals("The quick brown fox jumps", notes.getLore().get(0));
		assertEquals("over the lazy dog.", notes.getLore().get(1));
	}

	@Test
	public void WrongCommandDoesntRun() {
		Command cmd = new MockCommand("Patch");
		boolean result = commandExecutor.onCommand(player, cmd, label, args);
		assertEquals(false, result);
		assertEquals(0, noteManager.getNotes().size());
	}

	@Test
	public void DoesntRunForAnybodyElseEvenIfPlayerIsOP_OnlyNoobDad() {
		boolean result = commandExecutor.onCommand(player2, command, label, args);
		assertEquals(false, result);
		assertEquals(0, noteManager.getNotes().size());
	}
}