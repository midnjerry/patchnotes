package net.revmc.Revelations.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.revmc.Revelations.PatchNoteManager;

public class PatchNotesCommand implements CommandExecutor {
	private PatchNoteManager mNoteManager;

	public PatchNotesCommand(PatchNoteManager notes) {
		mNoteManager = notes;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (isNotValidInput(sender, cmd)) {
			return false;
		}
		mNoteManager.openMenu((Player) sender);
		return true;
	}

	private boolean isNotValidInput(CommandSender sender, Command cmd) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("Must be player to use this command.");
			return true;
		}

		if (!cmd.getName().equalsIgnoreCase("PatchNotes")) {
			return true;
		}
		return false;
	}

}
