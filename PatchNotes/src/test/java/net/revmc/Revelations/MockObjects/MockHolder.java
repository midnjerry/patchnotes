package net.revmc.Revelations.MockObjects;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class MockHolder implements InventoryHolder {

	Inventory mInventory;

	@Override
	public Inventory getInventory() {
		return mInventory;
	}

	public void setInventory(Inventory inv) {
		mInventory = inv;
	}

}
