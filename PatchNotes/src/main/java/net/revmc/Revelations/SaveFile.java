package net.revmc.Revelations;

import java.io.File;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;

public class SaveFile implements ISaveFile {
	private Plugin mPlugin;
	private File mFile;
	private final static String PATCH_NOTES_HEADER = "Data";
	private final static String ACCESS_TIMES_HEADER = "AccessTimes";

	public SaveFile(Plugin plugin) {
		mPlugin = plugin;
	}

	@Override
	public ArrayList<PatchNote> getAllPatchNoteEntries() {
		ArrayList<PatchNote> notes = new ArrayList<PatchNote>();
		if (mPlugin.getConfig().contains(PATCH_NOTES_HEADER)) {
			notes = (ArrayList<PatchNote>) mPlugin.getConfig().get(PATCH_NOTES_HEADER);
		}
		return notes;
	}

	@Override
	public void saveAllPatchNoteEntries(ArrayList<PatchNote> notes) {
		mPlugin.getConfig().set(PATCH_NOTES_HEADER, notes);
		mPlugin.saveConfig();
	}

	@Override
	public void safeFile() {
		File path = getPluginDataPath();
		openOrCreateConfigFile(path);
	}

	@Override
	public void saveAccessTimes(HashMap<UUID, OffsetDateTime> accessTimes) {
		Map<String, Object> convertedMap = convertMapForYAMLConsumption(accessTimes);
		mPlugin.getConfig().createSection(ACCESS_TIMES_HEADER, convertedMap);
		mPlugin.saveConfig();
	}

	@Override
	public HashMap<UUID, OffsetDateTime> getAccessTimes() {
		HashMap<UUID, OffsetDateTime> accessTimes = new HashMap<UUID, OffsetDateTime>();
		ConfigurationSection section = mPlugin.getConfig().getConfigurationSection(ACCESS_TIMES_HEADER);
		if (section != null) {
			Map<String, Object> map = section.getValues(false);
			accessTimes = convertMapFromYAMLConfig(map);
		}
		return accessTimes;
	}

	protected Map<String, Object> convertMapForYAMLConsumption(HashMap<UUID, OffsetDateTime> accessTimes) {
		Map<String, Object> result = new HashMap<String, Object>();
		for (UUID id : accessTimes.keySet()) {
			result.put(id.toString(), accessTimes.get(id).toString());
		}
		return result;
	}

	protected HashMap<UUID, OffsetDateTime> convertMapFromYAMLConfig(Map<String, Object> map) {
		HashMap<UUID, OffsetDateTime> result = new HashMap<UUID, OffsetDateTime>();
		for (String uuid_text : map.keySet()) {
			String datetime = (String) map.get(uuid_text);
			result.put(UUID.fromString(uuid_text), OffsetDateTime.parse(datetime));
		}
		return result;
	}

	protected File getPluginDataPath() {
		File path = mPlugin.getDataFolder();
		if (!path.exists()) {
			path.mkdirs();
		}
		return path;
	}

	protected void openOrCreateConfigFile(File path) {
		mFile = new File(path, "config.yml");
		if (!mFile.exists()) {
			mPlugin.saveConfig();
		}
	}
}
