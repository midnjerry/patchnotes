package net.revmc.Revelations;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import net.md_5.bungee.api.ChatColor;

public class PatchNote implements Comparable<PatchNote>, ConfigurationSerializable {
	private String mTitle;
	private OffsetDateTime mDateTime;
	private ArrayList<String> mLore;

	public PatchNote(OffsetDateTime datetime, ArrayList<String> data) {
		this(createTitle(datetime), datetime, data);
	}

	public PatchNote(String title, OffsetDateTime datetime, ArrayList<String> data) {
		mTitle = title;
		mDateTime = datetime;
		mLore = data;
	}

	public String getTitle() {
		return mTitle;
	}

	public ArrayList<String> getLore() {
		return mLore;
	}

	public OffsetDateTime getDateTime() {
		return mDateTime;
	}

	@Override
	public int compareTo(PatchNote compareWithNote) {
		// ascending order
		if (mDateTime.isAfter(compareWithNote.mDateTime)) {
			return 1;
		}
		if (mDateTime.isEqual(compareWithNote.mDateTime)) {
			return 0;
		}
		return -1;
	}

	private static String createTitle(OffsetDateTime datetime) {
		String result = ChatColor.GOLD + "Update " + datetime.format(DateTimeFormatter.ofPattern("MMM dd, uuuu"));
		return result;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("title", mTitle);
		map.put("timestamp", mDateTime.toString());
		map.put("lore", mLore);
		return map;
	}

	public static PatchNote deserialize(Map<String, Object> map) {
		String title = (String) map.get("title");
		OffsetDateTime dateTime = OffsetDateTime.parse((String) map.get("timestamp"));
		ArrayList<String> lore = (ArrayList<String>) map.get("lore");
		return new PatchNote(title, dateTime, lore);
	}
}
