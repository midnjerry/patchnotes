package net.revmc.Revelations.MockObjects;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class MockCommand extends Command {

	public MockCommand(String name) {
		super(name);
	}

	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		return false;
	}

}
