package net.revmc.Revelations.MockObjects;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import net.revmc.Revelations.gui.AbstractItemFactory;

public class MockItemStackFactory extends AbstractItemFactory {

	@Override
	public ItemStack getNewItem(Material material) {
		return new MockItemStack(material);
	}

}
