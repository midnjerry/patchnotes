package net.revmc.Revelations;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.UUID;
import org.bukkit.entity.Player;

import net.revmc.Revelations.gui.AbstractItemFactory;
import net.revmc.Revelations.gui.ItemStackFactory;
import net.revmc.Revelations.gui.Menu;

public class PatchNoteManager {
	HashMap<UUID, OffsetDateTime> mLastLogin;
	OffsetDateTime mLatestPatchTimeStamp;
	ArrayList<PatchNote> mNotes;
	ISaveFile mConfigFile;

	public PatchNoteManager(ISaveFile configFile) {
		mLastLogin = configFile.getAccessTimes();
		mNotes = configFile.getAllPatchNoteEntries();
		Collections.sort(mNotes);
		getLatestTimeStamp();
		mConfigFile = configFile;
	}

	private void getLatestTimeStamp() {
		if (mNotes != null && !mNotes.isEmpty()) {
			PatchNote note = mNotes.get(mNotes.size() - 1);
			mLatestPatchTimeStamp = note.getDateTime();
		}
	}

	public OffsetDateTime getLastLogin(Player player) {
		OffsetDateTime datetime = mLastLogin.get(player.getUniqueId());
		if (datetime == null) {
			datetime = OffsetDateTime.of(2000, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC);
			mLastLogin.put(player.getUniqueId(), datetime);
		}
		return datetime;
	}

	public boolean hasPlayerSeenAllUpdates(Player player) {
		if (mLatestPatchTimeStamp == null) {
			return true;
		}
		OffsetDateTime datetime = getLastLogin(player);
		return (datetime.isAfter(mLatestPatchTimeStamp));
	}

	public OffsetDateTime getLastPatchDateTime() {
		return mLatestPatchTimeStamp;
	}

	public void setLastDateTimeLogin(Player player, OffsetDateTime offsetDateTime) {
		mLastLogin.put(player.getUniqueId(), offsetDateTime);
	}

	public ArrayList<PatchNote> getNotes() {
		return mNotes;
	}

	public void openMenu(Player player) {
		openMenu(player, new ItemStackFactory());
	}

	protected void openMenu(Player player, AbstractItemFactory factory) {
		Menu menu = new Menu(getLastLogin(player), mNotes, factory);
		menu.open(player);
		setLastDateTimeLogin(player, OffsetDateTime.now());
		mConfigFile.saveAccessTimes(mLastLogin);
	}

	public boolean addNote(PatchNote patchNote) {
		boolean result = mNotes.add(patchNote);
		Collections.sort(mNotes);
		getLatestTimeStamp();
		mConfigFile.saveAllPatchNoteEntries(mNotes);
		return result;
	}

	public HashMap<UUID, OffsetDateTime> getAccessTimes() {
		return mLastLogin;
	}

}
