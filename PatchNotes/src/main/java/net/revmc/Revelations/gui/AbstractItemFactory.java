package net.revmc.Revelations.gui;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public abstract class AbstractItemFactory {
	public abstract ItemStack getNewItem(Material material);
}
