package net.revmc.Revelations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.File;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SaveFile.class)
public class ConfigTest {
	@Mock
	private Player player;
	private Player player2;
	private Player player3;
	private Plugin plugin;
	private File nonExistentFile;
	private File existingFile;
	private File directory;

	@Before
	public void setUp() throws Exception {
		directory = mock(File.class);

		player = mock(Player.class);
		when(player.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3A"));
		when(player.getName()).thenReturn("NoobDad");

		player2 = mock(Player.class);
		when(player2.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3B"));
		when(player2.getName()).thenReturn("Grezdaed");

		player3 = mock(Player.class);
		when(player3.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3C"));
		when(player3.getName()).thenReturn("TrinityStarr");

		plugin = mock(Plugin.class);
		when(plugin.getName()).thenReturn("PatchNotes");

		nonExistentFile = Mockito.mock(File.class);
		Mockito.when(nonExistentFile.exists()).thenReturn(false);

		existingFile = mock(File.class);
		Mockito.when(existingFile.exists()).thenReturn(true);

		// Trap constructor calls to return the mocked File-object
		PowerMockito.whenNew(File.class).withParameterTypes(String.class).withArguments(Matchers.anyString())
				.thenReturn(nonExistentFile);
	}

	private void SetupDirectoryDoesntExist() {
		when(directory.exists()).thenReturn(false);
		when(plugin.getDataFolder()).thenReturn(directory);
	}

	private void SetupDirectoryExists() {
		when(directory.exists()).thenReturn(true);
		when(plugin.getDataFolder()).thenReturn(directory);
	}

	private void SetupConfigDoesntExist() throws Exception {
		PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(nonExistentFile);
		when(directory.exists()).thenReturn(true);
		when(plugin.getDataFolder()).thenReturn(directory);
	}

	@Test
	public void TestDirectoryDoesntExistMock() {
		SetupDirectoryDoesntExist();
		File file = plugin.getDataFolder();
		assertEquals(false, file.exists());
	}

	@Test
	public void TestDirectoryExistsMock() {
		SetupDirectoryExists();
		File file = plugin.getDataFolder();
		assertEquals(true, file.exists());
	}

	@Test
	public void DirectoryCreatedIfDoesntExist() throws Exception {
		SetupDirectoryDoesntExist();
		SaveFile config = new SaveFile(plugin);
		config.getPluginDataPath();
		Mockito.verify(directory).mkdirs();
	}

	@Test
	public void IsBlankFileCreatedWhenNonExists() throws Exception {
		SetupDirectoryExists();
		SetupConfigDoesntExist();
		SaveFile classToTest = new SaveFile(plugin);
		classToTest.openOrCreateConfigFile(directory);
		PowerMockito.verifyNew(File.class).withArguments(directory, "config.yml");
		Mockito.verify(nonExistentFile).exists();

	}

	@Test
	public void AccessTimesCanBeSerializedAndDeserialized() throws Exception {
		SaveFile config = new SaveFile(plugin);
		HashMap<UUID, OffsetDateTime> accessTimes = new HashMap<UUID, OffsetDateTime>();
		OffsetDateTime date1 = OffsetDateTime.of(2017, 3, 10, 0, 0, 0, 0, ZoneOffset.UTC);
		OffsetDateTime date2 = OffsetDateTime.of(2017, 5, 4, 0, 0, 0, 0, ZoneOffset.UTC);
		OffsetDateTime date3 = OffsetDateTime.of(2017, 7, 25, 0, 0, 0, 0, ZoneOffset.UTC);
		accessTimes.put(player.getUniqueId(), date1);
		accessTimes.put(player2.getUniqueId(), date2);
		accessTimes.put(player3.getUniqueId(), date3);
		Map<String, Object> resultMap = config.convertMapForYAMLConsumption(accessTimes);
		assertEquals(date1.toString(), resultMap.get(player.getUniqueId().toString()));
		assertEquals(date2.toString(), resultMap.get(player2.getUniqueId().toString()));
		assertEquals(date3.toString(), resultMap.get(player3.getUniqueId().toString()));
		HashMap<UUID, OffsetDateTime> newMap = config.convertMapFromYAMLConfig(resultMap);
		assertEquals(accessTimes.size(), newMap.size());
		assertEquals(accessTimes.get(player.getUniqueId()), newMap.get(player.getUniqueId()));
		assertEquals(accessTimes.get(player2.getUniqueId()), newMap.get(player2.getUniqueId()));
		assertEquals(accessTimes.get(player3.getUniqueId()), newMap.get(player3.getUniqueId()));
	}
}