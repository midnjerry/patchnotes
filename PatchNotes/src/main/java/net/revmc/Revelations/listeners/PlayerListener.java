package net.revmc.Revelations.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.plugin.Plugin;

import net.revmc.Revelations.PatchNoteManager;
import net.revmc.Revelations.gui.NotesInventoryHolder;

public class PlayerListener implements Listener {
	private PatchNoteManager mNoteManager;
	private Plugin mPlugin;

	public PlayerListener(Plugin plugin, PatchNoteManager notes) {
		mNoteManager = notes;
		mPlugin = plugin;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		if (playerHasNotSeenLatestUpdate(event)) {
			event.setJoinMessage("A new Update is Available!  Type /patchnotes to see.");
			openMenuForNewPlayer(event);
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (thisInventoryIsACustomPatchNoteMenu(event)) {
			cancelNormalInventoryFunction(event);
		}
	}
	
	@EventHandler
	public void onPluginDisable(PluginDisableEvent event) {
		closeInventoriesForAllOnlinePlayers(event);
	}

	private boolean playerHasNotSeenLatestUpdate(PlayerJoinEvent event) {
		return !mNoteManager.hasPlayerSeenAllUpdates(event.getPlayer());
	}
	
	private void openMenuForNewPlayer(PlayerJoinEvent event) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(mPlugin, new Runnable() {
			@Override
			public void run() {
				event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10, 1);
				mNoteManager.openMenu(event.getPlayer());
			}
		},20);
	}	
	
	private void cancelNormalInventoryFunction(InventoryClickEvent event) {
		event.setCancelled(true);
	}

	private boolean thisInventoryIsACustomPatchNoteMenu(InventoryClickEvent event) {
		return event.getInventory().getHolder() instanceof NotesInventoryHolder;
	}

	private void closeInventoriesForAllOnlinePlayers(PluginDisableEvent event) {
		if (event.getPlugin().getName().equals(mPlugin.getName())) {
			for (Player player : Bukkit.getOnlinePlayers()) {
				player.closeInventory();
			}
		}
	}
}
