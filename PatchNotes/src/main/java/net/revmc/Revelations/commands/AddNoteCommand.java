package net.revmc.Revelations.commands;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.revmc.Revelations.PatchNote;
import net.revmc.Revelations.PatchNoteManager;

public class AddNoteCommand implements CommandExecutor {
	private PatchNoteManager mNoteManager;

	public AddNoteCommand(PatchNoteManager noteManager) {
		mNoteManager = noteManager;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (didNoobDadExecuteThisCommand(sender, command, args)) {
			return addPatchNote(args);
		}
		return false;
	}
	
	

	private boolean addPatchNote(String[] args) {
		ArrayList<String> data = parseArgs(args);
		String title = data.get(0);
		data.remove(0);
		PatchNote note = null;
		if (title == null || title.isEmpty()) {
			note = new PatchNote(OffsetDateTime.now(), data);
		} else {
			note = new PatchNote(title, OffsetDateTime.now(), data);
		}
		return mNoteManager.addNote(note);
	}

	private boolean didNoobDadExecuteThisCommand(CommandSender sender, Command command, String[] args) {
		boolean result = true;
		if (!command.getName().equalsIgnoreCase("AddNote")) {
			result = false;
		}

		if (args == null || args.length == 0) {
			sender.sendMessage("You must add a message to post!");
			result = false;
		}

		if (!(sender instanceof Player)) {
			sender.sendMessage("This command can only be executed as a player.");
			result = false;
		}

		Player player = (Player) sender;
		if (!player.getName().equals("NoobDad")) {
			sender.sendMessage("This command can only be executed by NoobDad.");
			result = false;
		}
		return result;
	}

	private ArrayList<String> parseArgs(String[] args) {
		ArrayList<String> result = new ArrayList<String>();
		String combinedString = String.join(" ", args);
		String[] parts = combinedString.split("\\|", 20);
		String title = "";
		String lore = "";
		if (hasTitle(parts)) {
			title = parts[0];
			if (title.endsWith(" ")) {
				title = title.substring(0, title.length() - 1);
			}
			lore = parts[1];
		} else {
			lore = parts[0];
		}
		result.add(title);
		result.addAll(parseLore(lore));
		return result;
	}

	private ArrayList<String> parseLore(String lore) {
		String[] parts = lore.split(" ");
		ArrayList<String> result = new ArrayList<String>();
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < parts.length; i++) {
			buffer.append(parts[i].trim());
			if (buffer.length() > 20) {
				result.add(buffer.toString().trim());
				buffer = new StringBuffer();
				continue;
			}
			if (i != parts.length - 1) {
				buffer.append(" ");
			}
		}
		result.add(buffer.toString());
		return result;
	}

	private boolean hasTitle(String[] parts) {
		return parts.length >= 2;
	}

}
