package net.revmc.Revelations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.plugin.Plugin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import net.revmc.Revelations.PatchNote;
import net.revmc.Revelations.PatchNoteManager;
import net.revmc.Revelations.MockObjects.MockConfig;
import net.revmc.Revelations.MockObjects.MockInventory;
import net.revmc.Revelations.MockObjects.MockInventoryView;
import net.revmc.Revelations.MockObjects.MockItemStackFactory;
import net.revmc.Revelations.gui.NotesInventoryHolder;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Bukkit.class)
public class PatchNoteManagerTest {
	@Mock
	private Player player;
	private Player player2;
	private ArrayList<PatchNote> notes;
	private PatchNoteManager noteManager;
	private MockInventoryView view;
	private Plugin plugin;

	@Before
	public void setUp() throws Exception {
		view = new MockInventoryView(player, new MockInventory(), new MockInventory(), InventoryType.CHEST);
		player = Mockito.mock(Player.class);
		when(player.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3A"));
		when(player.getName()).thenReturn("NoobDad");
		when(player.getOpenInventory()).thenReturn(view);

		player2 = Mockito.mock(Player.class);
		when(player2.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3B"));
		when(player2.getName()).thenReturn("Grezdaed");

		PowerMockito.mockStatic(Bukkit.class);
		when(Bukkit.createInventory(Mockito.any(), Mockito.anyInt(), Mockito.anyString()))
				.thenReturn(new MockInventory(new NotesInventoryHolder(), 56, "Updates for RevelationsMC!"));

		plugin = mock(Plugin.class);
		when(plugin.getName()).thenReturn("PatchNotes");

		ArrayList<String> lore = new ArrayList<String>();
		lore.add("Hello guys!");

		notes = new ArrayList<PatchNote>();
		notes.add(new PatchNote("Item A", OffsetDateTime.of(2017, 8, 1, 3, 20, 0, 0, ZoneOffset.UTC), lore));
		notes.add(new PatchNote("Item B", OffsetDateTime.of(2017, 9, 1, 3, 20, 0, 0, ZoneOffset.UTC), lore));
		notes.add(new PatchNote("Item C", OffsetDateTime.of(2017, 8, 15, 3, 20, 0, 0, ZoneOffset.UTC), lore));
		notes.add(new PatchNote("Item D", OffsetDateTime.of(2017, 8, 10, 3, 20, 0, 0, ZoneOffset.UTC), lore));
		notes.add(new PatchNote("Item E", OffsetDateTime.of(2017, 8, 10, 3, 21, 0, 0, ZoneOffset.UTC), lore));

		MockConfig config = new MockConfig(notes, new HashMap<UUID, OffsetDateTime>());
		noteManager = new PatchNoteManager(config);
		noteManager.setLastDateTimeLogin(player, OffsetDateTime.of(2017, 8, 30, 3, 20, 0, 0, ZoneOffset.UTC));
	}

	@Test
	public void UpdatesPlayerAccessOnMenuOpen() {
		OffsetDateTime datetime1 = noteManager.getLastLogin(player2);
		noteManager.openMenu(player2, new MockItemStackFactory());
		OffsetDateTime datetime2 = noteManager.getLastLogin(player2);
		assertEquals(true, datetime1.isBefore(datetime2));
	}

	@Test
	public void EmptyPatchNoteManagerShouldNotBreakTimeChecks() {
		MockConfig config = new MockConfig(new ArrayList<PatchNote>(), new HashMap<UUID, OffsetDateTime>());
		PatchNoteManager nManager = new PatchNoteManager(config);
		boolean result = nManager.hasPlayerSeenAllUpdates(player2);
		assertEquals(true, result);
	}

	@Test
	public void DontReturnNullForLastTimeLoginOfNewPlayer() {
		OffsetDateTime datetime = noteManager.getLastLogin(player2);
		assertNotNull(datetime);
		assertEquals(true, datetime.isBefore(OffsetDateTime.of(2000, 1, 1, 1, 30, 0, 0, ZoneOffset.UTC)));
	}

	@Test
	public void AreNotesSorted() {
		ArrayList<PatchNote> notes = noteManager.getNotes();
		assertEquals("Item A", notes.get(0).getTitle());
		assertEquals("Item D", notes.get(1).getTitle());
		assertEquals("Item E", notes.get(2).getTitle());
		assertEquals("Item C", notes.get(3).getTitle());
		assertEquals("Item B", notes.get(4).getTitle());
		assertEquals(notes.get(4).getDateTime(), noteManager.getLastPatchDateTime());
	}
}