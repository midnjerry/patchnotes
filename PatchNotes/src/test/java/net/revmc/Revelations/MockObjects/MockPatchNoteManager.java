package net.revmc.Revelations.MockObjects;

import org.bukkit.entity.Player;
import net.revmc.Revelations.ISaveFile;
import net.revmc.Revelations.PatchNoteManager;

public class MockPatchNoteManager extends PatchNoteManager {

	public MockPatchNoteManager(ISaveFile config) {
		super(config);
	}

	@Override
	public void openMenu(Player player) {

	}

}
