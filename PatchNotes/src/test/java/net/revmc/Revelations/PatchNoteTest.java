package net.revmc.Revelations;

import static org.junit.Assert.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Map;
import org.bukkit.ChatColor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import net.revmc.Revelations.PatchNote;

@RunWith(PowerMockRunner.class)
public class PatchNoteTest {
	@Mock
	private String title = "This is the title.";
	private OffsetDateTime datetime = OffsetDateTime.of(2017, 8, 1, 3, 20, 0, 0, ZoneOffset.UTC);
	private ArrayList<String> lore = new ArrayList<String>();
	private PatchNote note;

	@Before
	public void setUp() throws Exception {
		lore.add("Line 1");
		lore.add("Line 2");
		lore.add("Line 3");
		note = new PatchNote(title, datetime, lore);
	}

	@Test
	public void PatchNoteSerializesAndCanDeserialize() {
		Map<String, Object> map = note.serialize();
		PatchNote note2 = PatchNote.deserialize(map);
		assertEquals(title, note2.getTitle());
		assertEquals(datetime, note2.getDateTime());
		assertEquals(lore, note2.getLore());
	}

	@Test
	public void PatchNoteSerializesAndCanDeserializeChatColors() {
		lore.set(1, ChatColor.RED + "Line 1");
		lore.set(2, ChatColor.GOLD + "Line 2");
		note = new PatchNote(title, datetime, lore);
		Map<String, Object> map = note.serialize();
		PatchNote note2 = PatchNote.deserialize(map);
		assertEquals(title, note2.getTitle());
		assertEquals(datetime, note2.getDateTime());
		assertEquals(lore, note2.getLore());
	}
}