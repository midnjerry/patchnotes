package net.revmc.Revelations.MockObjects;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MockItemStack extends ItemStack {
	private ItemMeta pItemMeta;
	private boolean mHasMeta;

	public MockItemStack(Material material) {
		this(material, 1);
	}

	public MockItemStack(Material material, int num) {
		super(material, num);
		if (material != Material.AIR) {
			pItemMeta = new MockGenericItemMeta();
		} else {
			pItemMeta = null;
		}
		mHasMeta = false;
	}

	@Override
	public ItemMeta getItemMeta() {
		return pItemMeta;
	}

	@Override
	public boolean hasItemMeta() {
		return mHasMeta;
	}

	@Override
	public boolean setItemMeta(ItemMeta meta) {
		pItemMeta = meta;
		if (meta == null) {
			mHasMeta = false;
			if (super.getType() != Material.AIR) {
				pItemMeta = new MockGenericItemMeta();
			}
		} else {
			mHasMeta = true;
		}

		return true;
	}

}
