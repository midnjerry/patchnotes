package net.revmc.Revelations.gui;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import net.revmc.Revelations.PatchNote;

public class Menu {
	private OffsetDateTime mPlayerLastLogin;
	private ArrayList<PatchNote> mNotes;
	private AbstractItemFactory mFactory;

	public Menu(OffsetDateTime playerLastLogin, ArrayList<PatchNote> notes, AbstractItemFactory factory) {
		mPlayerLastLogin = playerLastLogin;
		mNotes = notes;
		if (mNotes == null) {
			mNotes = new ArrayList<PatchNote>();
		}
		mFactory = factory;
	}

	public void open(Player player) {
		player.openInventory(getMenu());
	}

	public Inventory getMenu() {
		Inventory inventory = createCustomInventory();
		populateIcons(inventory);
		return inventory;
	}

	private void populateIcons(Inventory inventory) {
		for (int i = 0; i < mNotes.size(); i++) {
			inventory.setItem(i, createItemStack(mNotes.get(i)));
		}
	}

	private Inventory createCustomInventory() {
		NotesInventoryHolder holder = new NotesInventoryHolder();
		Inventory inventory = Bukkit.createInventory(holder, 54, "Updates to RevelationsMC!");
		holder.setInventory(inventory);
		return inventory;
	}

	protected ItemStack createItemStack(PatchNote note) {
		ItemStack result = mFactory.getNewItem(Material.BOOK);
		if (mPlayerLastLogin.isBefore(note.getDateTime())) {
			result.setType(Material.ENCHANTED_BOOK);
		}
		ItemMeta meta = result.getItemMeta();
		meta.setDisplayName(note.getTitle());
		meta.setLore(note.getLore());
		result.setItemMeta(meta);
		return result;
	}
}
