package net.revmc.Revelations.listeners;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import net.revmc.Revelations.PatchNote;
import net.revmc.Revelations.MockObjects.MockConfig;
import net.revmc.Revelations.MockObjects.MockInventory;
import net.revmc.Revelations.MockObjects.MockInventoryView;
import net.revmc.Revelations.MockObjects.MockItemStackFactory;
import net.revmc.Revelations.MockObjects.MockPatchNoteManager;
import net.revmc.Revelations.gui.Menu;
import net.revmc.Revelations.gui.NotesInventoryHolder;
import net.revmc.Revelations.listeners.PlayerListener;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Bukkit.class)
public class PlayerListenerTest {
	@Mock
	private Player player;
	private Player player2;
	private Collection<? extends Player> players;
	private PlayerJoinEvent event;
	private PlayerListener listener;
	private MockPatchNoteManager noteManager;
	private MockInventoryView view;
	private MockInventoryView view2;
	private Plugin plugin;
	private MockItemStackFactory mFactory = new MockItemStackFactory();
	private BukkitScheduler scheduler;

	@Before
	public void setUp() throws Exception {
		mockPlayer();
		mockPlayer2();
		mockBukkit();
		mockPlugin();

		event = new PlayerJoinEvent(player, "test");
		ArrayList<PatchNote> notes = new ArrayList<PatchNote>();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("This is a test.");
		notes.add(new PatchNote(OffsetDateTime.of(2015, 9, 7, 2, 30, 0, 0, ZoneOffset.UTC), lore));
		MockConfig config = new MockConfig(notes, new HashMap<UUID, OffsetDateTime>());
		noteManager = new MockPatchNoteManager(config);
		listener = new PlayerListener(plugin, noteManager);
	}

	private void mockPlayer() {
		view = new MockInventoryView(player, new MockInventory(), new MockInventory(), InventoryType.CHEST);
		player = Mockito.mock(Player.class);
		when(player.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3A"));
		when(player.getName()).thenReturn("NoobDad");
		when(player.getOpenInventory()).thenReturn(view);
	}

	private void mockPlayer2() {
		view2 = new MockInventoryView(player2, new MockInventory(), new MockInventory(), InventoryType.CHEST);
		player2 = Mockito.mock(Player.class);
		when(player2.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3B"));
		when(player2.getName()).thenReturn("Grezdaed");
		when(player.getOpenInventory()).thenReturn(view2);
	}

	private void mockBukkit() throws Exception {
		scheduler = mock(BukkitScheduler.class);
		
		players = Arrays.asList(new Player[] { player, player2 });
		PowerMockito.mockStatic(Bukkit.class);
		when(Bukkit.createInventory(Mockito.any(), Mockito.anyInt(), Mockito.anyString()))
				.thenReturn(new MockInventory(new NotesInventoryHolder(), 56, "Updates for RevelationsMC!"));
		PowerMockito.doReturn(players).when(Bukkit.class, "getOnlinePlayers");
		when(Bukkit.getScheduler()).thenReturn(scheduler);
	}

	private void mockPlugin() {
		plugin = mock(Plugin.class);
		when(plugin.getName()).thenReturn("PatchNotes");
	}

	@Test
	public void DoesMockPlayerJoinEventWork() {
		assertEquals("test", event.getJoinMessage());
		assertEquals(player.getName(), event.getPlayer().getName());
	}

	@Test
	public void DoesListenerTellNewPlayerUpdateIsAvailable() {
		assertEquals("test", event.getJoinMessage());
		assertEquals(player.getName(), event.getPlayer().getName());
		listener.onPlayerJoin(event);
		assertNotNull(noteManager.getLastLogin(player));
		assertEquals("A new Update is Available!  Type /patchnotes to see.", event.getJoinMessage());
	}

	@Test
	public void DoesListenerTellOldPlayerUpdateIsAvailable() {
		noteManager.setLastDateTimeLogin(player, OffsetDateTime.of(2014, 9, 7, 3, 20, 0, 0, ZoneOffset.UTC));
		assertEquals(true, noteManager.getLastLogin(player).isBefore(noteManager.getLastPatchDateTime()));
		listener.onPlayerJoin(event);
		assertEquals("A new Update is Available!  Type /patchnotes to see.", event.getJoinMessage());
	}

	@Test
	public void DoesListenerIgnoreOldPlayerWhoHasSeenUpdates() {
		noteManager.setLastDateTimeLogin(player, OffsetDateTime.of(2018, 9, 7, 3, 20, 0, 0, ZoneOffset.UTC));
		String expectedValue = event.getJoinMessage();
		assertEquals("test", expectedValue);
		assertEquals(true, noteManager.getLastLogin(player).isAfter(noteManager.getLastPatchDateTime()));
		listener.onPlayerJoin(event);
		assertEquals(expectedValue, event.getJoinMessage());
	}

	@Test
	public void DoesMockInventoryClickEventWork() {
		InventoryClickEvent event = new InventoryClickEvent(view, SlotType.CONTAINER, 5, ClickType.LEFT,
				InventoryAction.PICKUP_ONE);
		assertEquals(false, event.isCancelled());
		assertEquals(SlotType.CONTAINER, event.getSlotType());
	}

	@Test
	public void DoesListenerCancelPatchNotesInventoryClickEvent() {
		Menu menu = new Menu(OffsetDateTime.of(2017, 8, 30, 3, 20, 0, 0, ZoneOffset.UTC), null, mFactory);
		Inventory inventory = menu.getMenu();
		view = new MockInventoryView(player, inventory, inventory, InventoryType.CHEST);
		InventoryClickEvent event = new InventoryClickEvent(view, SlotType.CONTAINER, 5, ClickType.LEFT,
				InventoryAction.PICKUP_ONE);
		assertEquals(false, event.isCancelled());
		assertEquals(SlotType.CONTAINER, event.getSlotType());
		listener.onInventoryClick(event);
		assertEquals(true, event.isCancelled());
	}

	@Test
	public void DoesListenerIgnorePatchNotesInventoryClickEvent() {
		InventoryClickEvent event = new InventoryClickEvent(view, SlotType.CONTAINER, 5, ClickType.LEFT,
				InventoryAction.PICKUP_ONE);
		assertEquals(false, event.isCancelled());
		assertEquals(SlotType.CONTAINER, event.getSlotType());
		listener.onInventoryClick(event);
		assertEquals(false, event.isCancelled());
	}

	@Test
	public void CloseInventoriesOnPluginDisable() {
		Menu menu = new Menu(OffsetDateTime.of(2017, 8, 30, 3, 20, 0, 0, ZoneOffset.UTC), null, mFactory);
		Inventory inventory = menu.getMenu();
		view = new MockInventoryView(player, inventory, inventory, InventoryType.CHEST);

		PluginDisableEvent disableEvent = new PluginDisableEvent(plugin);
		assertEquals(plugin, disableEvent.getPlugin());
		listener.onPluginDisable(disableEvent);
		Mockito.verify(player, Mockito.times(1)).closeInventory();
		Mockito.verify(player2, Mockito.times(1)).closeInventory();
	}

}