package net.revmc.Revelations.gui;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class NotesInventoryHolder implements InventoryHolder {

	private Inventory mInventory;

	@Override
	public Inventory getInventory() {
		return mInventory;
	}

	public void setInventory(Inventory inv) {
		mInventory = inv;
	}
}
