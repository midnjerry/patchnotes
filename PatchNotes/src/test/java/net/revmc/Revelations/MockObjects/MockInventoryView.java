package net.revmc.Revelations.MockObjects;

import org.bukkit.entity.HumanEntity;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;

public class MockInventoryView extends InventoryView {

	private Inventory mBottom;
	private Inventory mTop;
	private InventoryType mType;
	private HumanEntity mPlayer;

	public MockInventoryView(HumanEntity player, Inventory bottom, Inventory top, InventoryType type) {
		mBottom = bottom;
		mTop = top;
		mType = type;
		mPlayer = player;
	}

	@Override
	public Inventory getBottomInventory() {
		return mBottom;
	}

	@Override
	public HumanEntity getPlayer() {
		return mPlayer;
	}

	@Override
	public Inventory getTopInventory() {
		return mTop;
	}

	@Override
	public InventoryType getType() {
		return mType;
	}

}
