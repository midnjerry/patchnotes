package net.revmc.Revelations.MockObjects;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.meta.ItemMeta;

public class MockGenericItemMeta implements ItemMeta {

	private String mDisplayName;
	private List<String> mLore;

	@Override
	public Map<String, Object> serialize() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean addEnchant(Enchantment arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addItemFlags(ItemFlag... arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public ItemMeta clone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDisplayName() {
		return mDisplayName;
	}

	@Override
	public int getEnchantLevel(Enchantment arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map<Enchantment, Integer> getEnchants() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<ItemFlag> getItemFlags() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLocalizedName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getLore() {
		return mLore;
	}

	@Override
	public boolean hasConflictingEnchant(Enchantment arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDisplayName() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasEnchant(Enchantment arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasEnchants() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasItemFlag(ItemFlag arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasLocalizedName() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasLore() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isUnbreakable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeEnchant(Enchantment arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeItemFlags(ItemFlag... arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDisplayName(String name) {
		mDisplayName = name;
	}

	@Override
	public void setLocalizedName(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setLore(List<String> lore) {
		mLore = lore;
	}

	@Override
	public void setUnbreakable(boolean arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public Spigot spigot() {
		// TODO Auto-generated method stub
		return null;
	}

}
