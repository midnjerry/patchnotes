package net.revmc.Revelations.MockObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;


public class MockInventory implements Inventory {
	private int SIZE = 56;
	private HashMap<Integer, ItemStack> items;
	private InventoryHolder mHolder;
	private String mTitle;

	public MockInventory() {
		this(null, 56, "");	
	}
	
	public MockInventory(InventoryHolder holder, int size, String title){
		SIZE = size;
		mHolder = holder;
		mTitle = title;
		
		items = new HashMap<Integer, ItemStack>();
		for (int i = 0; i < SIZE; i++) {
			items.put(i, null);
		}
	}

	@Override
	public HashMap<Integer, ItemStack> addItem(ItemStack... arg0) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ? extends ItemStack> all(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ? extends ItemStack> all(Material arg0) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ? extends ItemStack> all(ItemStack arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public void clear(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean contains(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean contains(Material arg0) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean contains(ItemStack arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean contains(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean contains(Material arg0, int arg1) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean contains(ItemStack arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAtLeast(ItemStack arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int first(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int first(Material arg0) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int first(ItemStack arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int firstEmpty() {
		for (Integer index : items.keySet()) {
			if (items.get(index) == null) {
				return index;
			}
		}
		return -1;
	}

	@Override
	public ItemStack[] getContents() {
		ArrayList<ItemStack> contents = new ArrayList<ItemStack>();
		for (int i = 0; i < SIZE; i++) {
			contents.add(items.get(i));
		}
		return contents.toArray(new ItemStack[contents.size()]);
	}

	@Override
	public ItemStack getItem(int slot) {
		return items.get(slot);
	}

	@Override
	public Location getLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getMaxStackSize() {
		return 64;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSize() {
		return items.size();
	}

	@Override
	public ItemStack[] getStorageContents() {
		ArrayList<ItemStack> storageContents = new ArrayList<ItemStack>();
		for (int i = 0; i < 36; i++) {
			storageContents.add(items.get(i));
		}
		return storageContents.toArray(new ItemStack[storageContents.size()]);
	}

	@Override
	public String getTitle() {
		return mTitle;
	}

	@Override
	public InventoryType getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HumanEntity> getViewers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<ItemStack> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<ItemStack> iterator(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(Material arg0) throws IllegalArgumentException {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(ItemStack arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public HashMap<Integer, ItemStack> removeItem(ItemStack... arg0) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setContents(ItemStack[] contents) throws IllegalArgumentException {
		if (contents.length != SIZE)
			throw new IllegalArgumentException("Content size is not " + SIZE);

		for (int i = 0; i < SIZE; i++) {
			items.put(i, contents[i]);
		}
	}

	@Override
	public void setMaxStackSize(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setStorageContents(ItemStack[] arg0) throws IllegalArgumentException {
		// TODO Auto-generated method stub
	}

	@Override
	public InventoryHolder getHolder() {
		return mHolder;
	}

	@Override
	public void setItem(int slot, ItemStack item) {
		items.put(slot, item);
	}
}
