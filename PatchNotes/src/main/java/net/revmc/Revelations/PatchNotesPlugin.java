package net.revmc.Revelations;

import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;
import net.revmc.Revelations.commands.AddNoteCommand;
import net.revmc.Revelations.commands.PatchNotesCommand;
import net.revmc.Revelations.listeners.PlayerListener;

public class PatchNotesPlugin extends JavaPlugin {
	private PatchNoteManager mNoteManager;
	private ISaveFile mPatchNotesFile;

	public void onEnable() {
		registerClassesForDeserialization();
		
		mPatchNotesFile = new SaveFile(this);
		mPatchNotesFile.safeFile();
		mNoteManager = new PatchNoteManager(mPatchNotesFile);
		
		registerEvents();
		registerCommands();
	}
	
	private void registerClassesForDeserialization() {
		ConfigurationSerialization.registerClass(PatchNote.class);
	}

	private void registerEvents() {
		this.getServer().getPluginManager().registerEvents(new PlayerListener(this, mNoteManager), this);
	}

	private void registerCommands() {
		getCommand("PatchNotes").setExecutor(new PatchNotesCommand(mNoteManager));
		getCommand("AddNote").setExecutor(new AddNoteCommand(mNoteManager));
	}
}
