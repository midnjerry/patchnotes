package net.revmc.Revelations.MockObjects;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import net.revmc.Revelations.ISaveFile;
import net.revmc.Revelations.PatchNote;

public class MockConfig implements ISaveFile {
	private ArrayList<PatchNote> entries;
	private HashMap<UUID, OffsetDateTime> accessTimes;

	public MockConfig(ArrayList<PatchNote> notes, HashMap<UUID, OffsetDateTime> times) {
		entries = notes;
		accessTimes = times;
	}

	@Override
	public ArrayList<PatchNote> getAllPatchNoteEntries() {
		return entries;
	}

	@Override
	public void saveAllPatchNoteEntries(ArrayList<PatchNote> notes) {
		entries = notes;
	}

	@Override
	public void safeFile() {
	}

	@Override
	public void saveAccessTimes(HashMap<UUID, OffsetDateTime> accessTimes) {
		this.accessTimes = accessTimes;
	}

	@Override
	public HashMap<UUID, OffsetDateTime> getAccessTimes() {
		return accessTimes;
	}

}
