package net.revmc.Revelations.gui;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemStackFactory extends AbstractItemFactory {

	@Override
	public ItemStack getNewItem(Material material) {
		return new ItemStack(material);
	}
}
